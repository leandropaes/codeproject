<?php

Route::get('/', function () {
    return view('app');
});

Route::post('oauth/access_token', function(){
    return Response::json(Authorizer::issueAccessToken());
});

Route::group(['middleware' => 'oauth'], function(){

    Route::resource('client', 'ClientController', ['except' => ['create', 'edit']]);

    Route::resource('project', 'ProjectController', ['except' => ['create', 'edit']]);
    Route::resource('project.note', 'ProjectNoteController', ['except' => ['create', 'edit']]);
    Route::resource('project.tasks', 'ProjectTaskController', ['except' => ['create', 'edit']]);

    Route::group(['prefix' => 'project'], function(){
        Route::get('{id}/members', 'ProjectController@members');
        Route::post('{id}/members', 'ProjectController@addMember');
        Route::get('{id}/ismember/{memberId}', 'ProjectController@ismember');
        Route::delete('{id}/members/{memberId}', 'ProjectController@removeMember');

        Route::post('{id}/file', 'ProjectFileController@store');
        Route::delete('{id}/file/{fileId}', 'ProjectFileController@destroy');
    });

});