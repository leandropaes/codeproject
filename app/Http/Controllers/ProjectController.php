<?php

namespace CodeProject\Http\Controllers;

use CodeProject\Repositories\ProjectMemberRepository;
use CodeProject\Repositories\ProjectRepository;
use CodeProject\Services\ProjectService;
use Illuminate\Http\Request;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

class ProjectController extends Controller
{
    /**
     * @var ProjectRepository
     */
    private $repository;

    /**
     * @var ProjectMemberRepository
     */
    protected $repositoryMember;

    /**
     * @var ProjectService
     */
    private $service;

    /**
     * @param ProjectRepository $repository
     * @param ProjectService $service
     */
    public function __construct(ProjectRepository $repository, ProjectService $service, ProjectMemberRepository $repositoryMember)
    {
        $this->repository = $repository;
        $this->service = $service;
        $this->repositoryMember = $repositoryMember;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return $this->repository->with(['owner', 'client'])->findWhere(['owner_id' => Authorizer::getResourceOwnerId()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        return $this->service->create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        if ($this->checkProjectOwner($id) == false) {
            return ['error' => 'Access Forbidden'];
        }
        return $this->repository->with(['owner', 'client'])->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if ($this->checkProjectOwner($id) == false) {
            return ['error' => 'Access Forbidden'];
        }
        return $this->service->update($request->all(), $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->checkProjectOwner($id) == false) {
            return ['error' => 'Access Forbidden'];
        }
        $this->repository->delete($id);
    }

    /**
     * Lista membros do projeto
     *
     * @param $id
     * @return mixed
     */
    public function members($id)
    {
        return $this->repository->find($id);
        //return $this->repositoryMember->with(['user'])->findByField('project_id', $id);
    }

    /**
     * Cadastra membro para o projeto
     *
     * @param Request $request
     * @return array|mixed
     */
    public function addMember(Request $request)
    {
        return $this->service->addMember($request->all());
    }

    /**
     * Verifica se é membro do projeto
     *
     * @param $id
     * @param $memberId
     * @return array
     */
    public function isMember($id, $memberId)
    {
        return ['success' => $this->repositoryMember->isMember($id, $memberId)];
    }

    public function removeMember($id, $memberId)
    {
        $this->service->removeMember($id, $memberId);
    }

    private function checkProjectOwner($projectId)
    {
        $userId = Authorizer::getResourceOwnerId();
        return $this->repository->isOwner($projectId, $userId);
    }
}
