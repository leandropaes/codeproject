<?php

namespace CodeProject\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectMemberRepository
 * @package namespace CodeProject\Repositories;
 */
interface ProjectMemberRepository extends RepositoryInterface
{
    public function isMember($projectId, $memberId);
    public function removeMember($projectId, $memberId);
}
