<?php

namespace CodeProject\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use CodeProject\Entities\ProjectMember;

/**
 * Class ProjectMemberRepositoryEloquent
 * @package namespace CodeProject\Repositories;
 */
class ProjectMemberRepositoryEloquent extends BaseRepository implements ProjectMemberRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProjectMember::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria( app(RequestCriteria::class) );
    }

    /**
     * Remove membros de um projeto
     *
     * @param $projectId
     * @param $memberId
     * @return mixed
     */
    public function removeMember($projectId, $memberId)
    {
        return ProjectMember::where('project_id', $projectId)->where('member_id', $memberId)->delete();
    }

    /**
     * @param $projectId
     * @param $memberId
     * @return bool
     */
    public function isMember($projectId, $memberId)
    {
        if (count($this->findWhere(['project_id' => $projectId, 'member_id' => $memberId])))
        {
            return true;
        }
        return false;
    }
}