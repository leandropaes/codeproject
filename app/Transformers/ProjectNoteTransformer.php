<?php

namespace CodeProject\Transformers;

use CodeProject\Entities\ProjectNote;
use League\Fractal\TransformerAbstract;

class ProjectNoteTransformer extends TransformerAbstract
{
    public function transform(ProjectNote $note)
    {
        return [
            'project_note_id' => $note->id,
            'project_id' => $note->id,
            'title' => $note->title,
            'note' => $note->note,
            'created_at' => $note->created_at,
            'updated_at' => $note->updated_at,
        ];
    }
}