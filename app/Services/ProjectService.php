<?php

namespace CodeProject\Services;

use CodeProject\Repositories\ProjectMemberRepository;
use CodeProject\Repositories\ProjectRepository;
use CodeProject\Validators\ProjectFileValidator;
use CodeProject\Validators\ProjectMemberValidator;
use CodeProject\Validators\ProjectValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Contracts\Filesystem\Factory as Storage;
use Prettus\Validator\Exceptions\ValidatorException;

class ProjectService
{
    /**
     * @var ProjectRepository
     */
    protected $repository;

    /**
     * @var ProjectValidator
     */
    protected $validator;

    /**
     * @var ProjectMemberRepository
     */
    protected $repositoryMember;

    /**
     * @var ProjectMemberValidator
     */
    protected $validatorMember;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var Storage
     */
    private $storage;
    /**
     * @var ProjectFileValidator
     */
    private $validatorFile;

    public function __construct(ProjectRepository $repository, ProjectValidator $validator, ProjectMemberRepository $repositoryMember, ProjectMemberValidator $validatorMember, Filesystem $filesystem, Storage $storage, ProjectFileValidator $validatorFile)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->repositoryMember = $repositoryMember;
        $this->validatorMember = $validatorMember;
        $this->filesystem = $filesystem;
        $this->storage = $storage;
        $this->validatorFile = $validatorFile;
    }

    public function create(array $data)
    {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);

        } catch(ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }
    }

    public function update(array $data, $id)
    {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->update($data, $id);

        } catch(ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }
    }

    public function addMember(array $data)
    {
        try {
            $this->validatorMember->with($data)->passesOrFail();
            return $this->repositoryMember->create($data);

        } catch(ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }
    }

    public function removeMember($projectId, $memberId)
    {
        $this->repositoryMember->removeMember($projectId, $memberId);
    }

    public function createFile(array $data)
    {
        try {
            $this->validatorFile->with($data)->passesOrFail();

            $project = $this->repository->skipPresenter()->find($data['project_id']);
            $projectFile = $project->files()->create($data);

            $this->storage->put($projectFile->id . "." . $data['extension'], $this->filesystem->get($data['file']));

        } catch(ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }

    }

    public function removeFile($projectId, $fileId)
    {
        try {
            $project = $this->repository->skipPresenter()->find($projectId);
            $projectFile = $project->files()->findOrFail($fileId);

            if ($this->storage->exists($projectFile->id . "." . $projectFile->extension))
            {
                $this->storage->delete($projectFile->id . "." . $projectFile->extension);
                $project->files()->delete($fileId);
            }

        } catch(ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }



    }
}